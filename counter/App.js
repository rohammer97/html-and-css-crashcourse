import './App.css';
import React, {useState} from "react";
import {Weather} from "./Weather";
import {Counter} from "./Counter";
import {Clock} from "./Clock";

function App() {

    let name = 'Rocky'

    // let appNumber = 15
    const [appNumber, setAppNumber] = useState(15)

    const [addressFromChild, setAddressFromChild] = useState('Beijing')

    const [numberFromCounter, setNumberFromCounter] = useState(null)

    const updateNumber = (dataFromChild) => {setNumberFromCounter(dataFromChild)}
    // const getAddressFromChild = (cityFromChild) => {
    //     setAddressFromChild(cityFromChild)
    // }
    console.log('App is rendered')

    return (
        <React.Fragment>
            <div className="App">
                <h1 onClick={() => {setAppNumber(appNumber + 1) }}>
                    The date is {appNumber}
                </h1>
                <h2>The city is {addressFromChild}</h2>
                <h1>Number from Counter is {numberFromCounter}</h1>
            </div>

            <Weather date = {appNumber} updateCity = {setAddressFromChild}/>
            <hr/>
            <Counter updateNumber = {updateNumber}/>
            <hr/>
            <Clock/>
        </React.Fragment>
    );
}


// function Weather() {
//     return (
//         <div>
//             <h2>Toronto</h2>
//             <p>Weather details</p>
//         </div>
//     )
// }


export default App;
