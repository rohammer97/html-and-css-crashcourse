import React, {useState} from "react";

function Weather({date, updateCity}) {
    console.log('Weather is rendered')
    const [city, setCity] = useState('Toronto')

    return (
        <div>
            <h2>From Weather file</h2>
            <h2 onClick={() => {updateCity(city)}}>{city}</h2>
            <h2>Date: {date}</h2>
            <p>Weather details</p>
        </div>
    )
}

export {Weather}