import {useEffect, useState} from "react";

export function Clock() {

    // let date = new Date()
    const [date, setDate] = useState(new Date())

    useEffect(() => {
        let intervalId = setInterval(() => {setDate(new Date)}, 1000)

        // call the return when the component is about to be unmounted
        return (() => {clearInterval(intervalId)})
        // return ({clearInterval})
    }, [])

    return (
        <div>
            <h1>
                {`${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`}
            </h1>
        </div>
    )
}