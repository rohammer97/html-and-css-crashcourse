import {useEffect, useState} from "react";

export function Counter({updateNumber}) {
    console.log('Counter is rendered')
    const [counterNumber, setCounterNumber] = useState(0)

    useEffect(() => {
        updateNumber(counterNumber)
    }, [counterNumber])

    const increaseNum = () => {
        // setState setter is an async function
        // better way to update state
        setCounterNumber(a => a + 1)
        // updateNumber(counterNumber)
    }

    // const [counterNumber, setCounterNumber] = useState(0)
    return(
        <>
            <h1>{counterNumber}</h1>
            <button onClick={increaseNum}>Click me to +1</button>
        </>
    )
}